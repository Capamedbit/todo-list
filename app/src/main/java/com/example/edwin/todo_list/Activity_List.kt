package com.example.edwin.todo_list

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase

import kotlinx.android.synthetic.main.activity__list.*
import java.util.*

class Activity_List : AppCompatActivity() {

    lateinit var listRecyclerView: RecyclerView

    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("todo-list");

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__list)
        setSupportActionBar(toolbar)

        //Recycler VIew
        //Conectar el recycle view de la vista al de la actividad
        listRecyclerView = findViewById(R.id.list_recycler_view)
        //vamos a utilizar en linea ....vetical ver horizaonal
        listRecyclerView.layoutManager = LinearLayoutManager(this)

        listRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref)

        fab.setOnClickListener { view ->
            showCreateListDialog()
        }
    }

    private fun showCreateListDialog(){
        //take the strings variables
        val dialogTitle = getString(R.string.name_of_list)
        val positiveButtonTitle = getString(R.string.create_list)

        val builder = AlertDialog.Builder(this)

        //create a view of type editTEXT
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        //put the tittle in the builder
        builder.setTitle(dialogTitle)
        //the list
        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle){dialog, i ->
            val newList = listTitleEditText.text.toString()
            //la llave todoList, con el .child()-> que sea único= SI NO EXISTE SE CREA
            //SIEMPRE PARA TRABJAR ID´S desde nuestro codigo usar UUID
            //cada vez que creamos un nuevo item se genera un nuevo UUID
            ref.child(UUID.randomUUID().toString()).child("List-name").setValue(newList)
            dialog.dismiss()
        }

        builder.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_activity__list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
