package com.example.edwin.todo_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import java.text.FieldPosition
import java.util.ArrayList

class ListSelectionRecyclerViewAdapter(ref:DatabaseReference): //
    RecyclerView.Adapter<ListSelectionRecyclerViewHolder>() {

    //No vamos a usar esta lista quemada si no vamos a crear nosotros mismo las listas
    // val listTitles = arrayOf("Shopping List","Chores","Homework")

    val todoLists: ArrayList<TodoList> = arrayListOf()
    init{
        ref.addChildEventListener(object :ChildEventListener{
            override fun onCancelled(item: DatabaseError) {//ERROR

            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {//SE MUEVA UNO DE LOS ITEMS
                 }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {//CUANDO SE CAMBIO ...TODOS DEVUELVEN UN SNAPSHOT, LLAVE VALOR
                 }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {//CUANDO SE AÑADA UN ITEM

                //spanshot: es 123q4324238423746823647 esl valor  o nombre es el ITEM "LAPIZ"


                //EN EL PATH DEL LIST-NAME, DEBE SER COMO ESTA EN EL FIREBASE
            val listTitle = item.child("List-name").value.toString()//lo hacemos para poder guardar a nuesta lista de STRINGS
                val listId = item.key.toString()

                todoLists.add(TodoList(listId,listTitle))//cuando se cree una referencia a un hijo de la base de datos
               // notifyDataSetChanged()//para decir al adapter que el data cambio y esto le va a notificar, para que se actulice
                notifyItemInserted(todoLists.size)
            }

            override fun onChildRemoved(item: DataSnapshot) {//CUANDO SE REMUEVA UN ITEM
                val deltedIndex = todoLists.indexOfFirst { element-> element.id == item.key } //it.id == item.key --> para abreviar
                //el indexofFirt se ejecuat muchas veces. ELEMENT ES CADA UNO ITEMS DE mi lista
                todoLists.removeAt(deltedIndex) //elimino del view holder
                notifyItemRemoved(deltedIndex)//elimino de la lista
               }
            //se elemina dependiendo el KEY,
        })
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListSelectionRecyclerViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_selection_view_holder,parent,false)//genrar una insancia para cada uno de los view holders
        return ListSelectionRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {//devolvemos cuantas filas voya tener...es decir el nuemro de elementos del arrelos
        return todoLists.count()//To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: ListSelectionRecyclerViewHolder, position: Int) {
        holder.listTitle.text= todoLists[position].listName //se va a repetir el nuermo de veces qde items que tengamos

    }

}


//notaaa: para haer la peticion debemos de poner el ID y luego la llave valor List.name y el valor ose ael item